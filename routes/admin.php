<?php

use App\Http\Controllers\AdminControllers\AuthController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminControllers\DashboardController;


Route::get('/login', [AuthController::class, 'login_page'])->name('admin.login_page');
Route::post('/login', [AuthController::class, 'login'])->name('admin.login');




/*All Admin Routes List*/
Route::middleware(['auth', 'user-access:admin'])->namespace('App\Http\Controllers\AdminControllers')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('admin.dashboard');

    //students
    Route::resource('students', 'StudentController');
    //teachers
    Route::resource('teachers', 'TeacherController');
    //extensions
    Route::resource('extensions', 'ExtensionController');
    //levels
    Route::resource('levels', 'LevelController');
    //categories
    Route::resource('categories', 'CategoryController');
    //skills
    Route::resource('skills', 'SkillController');
    //courses
    Route::resource('courses', 'CourseController');

    //curriculums
    Route::resource('curriculums', 'CurriculumController');

    //scheduled
    Route::resource('scheduleds', 'ScheduledController');

    //units
    Route::resource('units', 'UnitController');

    //lessons
    Route::resource('lessons', 'LessonController');

    Route::get('moduleFile/destroy/{id}', [\App\Http\Controllers\AdminControllers\CurriculumController::class,'moduleFileDestroy'])->name('moduleFile.destroy');


    //Sprint 3
    //calendars
    Route::resource('calendars', 'CalendarController');
    //calendar Questions
    Route::resource('calendar_questions', 'CalendarQuestionController');
    //calendar answers
    Route::resource('calendar_answers', 'CalendarAnswerController');



});




