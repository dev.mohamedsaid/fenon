@extends('website.layout.master')

@section('page_title')  المجالات والمحاور الفنية| {{$content->title}} @endsection
@section('content')

    @include('website.layout.inner-header')
    <!--course section start-->
    <section class="section-padding page bg-light">

        @include('website.layout.live_search')

        <div class="container">
            <div class="row live-search-list">

                @forelse($content->courses as $con)
                    @include('website.includes.course')
                @empty
                    @include('website.layout.no_data')
                @endforelse
            </div>

        </div>
        </div>
        <!--course-->
    </section>

@endsection
