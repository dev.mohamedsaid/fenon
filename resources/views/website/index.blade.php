@extends('website.layout.master')

@section('page_title') الصفحة الرئيسية  @endsection
@section('content')

<section
    class="banner-style-4 banner-padding px-4"
    style="
        background: linear-gradient(to bottom left, #02280fd4, #4a0f07bc),
          url({{ asset('frontend/assets/images/main-bg.jpeg')}});
      "
>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-xl-6 col-lg-6 text-lg-start text-center">
                <div class="academy-logos">
                    <img src="{{ asset('frontend/assets/images/university.png')}}" alt="Mohamed Nasr" />
                    <span class="mx-1"></span>
                    <img src="{{ asset('frontend/assets/images/faculty.png')}}" alt="Mohamed Nasr" />
                </div>
                <div class="banner-content">
                    <h3 class="text-white mb-30" style="line-height: 40px">
                        فعالية استراتيجية التعلم المعكوس باستخدام الوسائط المعلوماتية فى
                        تنمية بعض المهارات التصميمية الرقمية لدى تلاميذ المرحلة
                        الابتدائية
                    </h3>
                    <div
                        class="mb-40 d-flex flex-column align-items-lg-start align-items-center"
                    >
                        <p class="d-flex mb-2">
                            <i class="far fa-user me-3 prelative-3px"></i>
                            <span>اسم الباحث : محمد نصر السيد مصطفى العادلى</span>
                        </p>
                        <p class="d-flex">
                            <i class="far fa-car-building me-3 prelative-3px"></i>
                            <span
                            >جامعة بور سعيد ، كلية التربية النوعية ، قسم العلوم التربوية
                    و النفسية</span
                            >
                        </p>
                    </div>

                    <div
                        class="btn-container d-flex flex-sm-row flex-column justify-content-lg-start justify-content-center"
                    >
                        <a href="#goalSection" class="btn btn-main rounded mb-sm-0 mb-3"
                        >أهداف البحث</a
                        >
                        <a href="#" class="btn btn-white rounded ms-sm-2">عن الباحث</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xl-6 col-lg-6 banner-column-fix">
                <div class="banner-img-round mt-5 mt-lg-0 mx-auto">
                    <img
                        src="{{ asset('frontend/assets/images/research/nasr.png')}}"
                        alt="Mohamed Nasr"
                        class="img-fluid"
                    />
                    <img
                        src="{{ asset('frontend/assets/images/pic-frame.png')}}"
                        alt="Mohamed Nasr"
                        class="frame"
                    />
                </div>
            </div>
        </div>
        <!-- / .row -->
    </div>
    <!-- / .container -->
</section>
<section class="counter-section4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 counter-inner">
                <div class="row">
                    <div class="col-lg-3 col-md-6 d-flex justify-content-center">
                        <div class="staff-img-item mb-5 mb-lg-0">
                            <div class="image mx-auto">
                                <img src="{{ asset('frontend/assets/images/research/04.png')}}" alt="research" />
                                <img
                                    src="{{ asset('frontend/assets/images/pic-frame.png')}}"
                                    alt="Frame"
                                    class="frame"
                                />
                            </div>
                            <div class="text mt-5 pt-3">
                                <h5 class="text-white mb-3">أ.د / جمال السيد تفاحة</h5>
                                <p class="desc">
                                    أستاذ الصحة النفسية المتفرغ بقسم العلوم التربوية و النفسية
                                    بكلية التربية النوعية جامعة بور سعيد
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex justify-content-center">
                        <div class="staff-img-item mb-5 mb-lg-0">
                            <div class="image mx-auto">
                                <img src="{{ asset('frontend/assets/images/research/03.png')}}" alt="research" />
                                <img
                                    src="{{ asset('frontend/assets/images/pic-frame.png')}}"
                                    alt="Frame"
                                    class="frame"
                                />
                            </div>
                            <div class="text mt-5 pt-3">
                                <h5 class="text-white mb-3">أ.م.د / سامية يوسف صالح</h5>
                                <p class="desc">
                                    أستاذ مساعد متفرغ بقسم العلوم التربوية و النفسية بكلية
                                    التربية النوعية جامعة بور سعيد
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex justify-content-center">
                        <div class="staff-img-item mb-5 mb-lg-0">
                            <div class="image mx-auto">
                                <img
                                    class="fit-contain"
                                    src="{{ asset('frontend/assets/images/research/02.png')}}"
                                    alt="research"
                                />
                                <img
                                    src="{{ asset('frontend/assets/images/pic-frame.png')}}"
                                    alt="Frame"
                                    class="frame"
                                />
                            </div>
                            <div class="text mt-5 pt-3">
                                <h5 class="text-white mb-3">
                                    أ.م.د / محمد عبد الرحمن السعدنى
                                </h5>
                                <p class="desc">
                                    أستاذ مساعد متفرغ بقسم العلوم التربوية و النفسية بكلية
                                    التربية النوعية جامعة بور سعيد
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 d-flex justify-content-center">
                        <div class="staff-img-item mb-5 mb-lg-0">
                            <div class="image mx-auto">
                                <img src="{{ asset('frontend/assets/images/research/01.png')}}" alt="research" />
                                <img
                                    src="{{ asset('frontend/assets/images/pic-frame.png')}}"
                                    alt="Frame"
                                    class="frame"
                                />
                            </div>
                            <div class="text mt-5 pt-3">
                                <h5 class="text-white mb-3">أ.م.د / عمرو أحمد الأطروش</h5>
                                <p class="desc">
                                    أستاذ مساعد بقسم التربية الفنية بكلية التربية النوعية
                                    جامعة بور سعيد
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section
    class="features-3 section-padding-top section-padding-btm"
    id="goalSection"
>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="section-heading mb-50 text-center">
                    <h2 class="font-lg mb-3">عــن البحث و أهدافه</h2>
                    <p class="text-secondary" style="font-size: 18px">
                        استكمــــالاً للحصـــــول على درجـــــة دكتــوراة الفلسفـــة فى
                        <br />
                        مناهج و طرق تدريس التربية الفنية
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-6 col-xl-3 col-sm-6">
                <div
                    class="feature-item feature-style-top hover-shadow rounded border-0"
                >
                    <div class="feature-icon">
                        <i class="fa fa-check-circle"></i>
                    </div>
                    <div class="feature-text">
                        <h4>الهدف الأول</h4>
                        <p>تحديد بعض المهارات التصميمية الرقمية لتلاميذ الصف السادس</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xl-3 col-sm-6">
                <div
                    class="feature-item feature-style-top hover-shadow rounded border-0"
                >
                    <div class="feature-icon">
                        <i class="fa fa-check-circle"></i>
                    </div>
                    <div class="feature-text">
                        <h4>الهدف الثانى</h4>
                        <p>إعداد البرنامج التعليمى للتعلم المعكوس لتنميتها</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xl-3 col-sm-6">
                <div
                    class="feature-item feature-style-top hover-shadow rounded border-0"
                >
                    <div class="feature-icon">
                        <i class="fa fa-check-circle"></i>
                    </div>
                    <div class="feature-text">
                        <h4>الهدف الثالث</h4>
                        <p>
                            معرفة تأثير استخدام البرنامج على القدرات الآدائية المهارية
                            للتلاميذ
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xl-3 col-sm-6">
                <div
                    class="feature-item feature-style-top hover-shadow rounded border-0"
                >
                    <div class="feature-icon">
                        <i class="fa fa-check-circle"></i>
                    </div>
                    <div class="feature-text">
                        <h4>الهدف الرابع</h4>
                        <p>
                            معرفة تأثير استخدام البرنامج على القدرات المعرفية للتلاميذ
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@if(count($skills) > 0)
<section class="course-wrapper section-padding bg-gray">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="section-heading mb-70 text-center">
                    <h2 class="font-lg">قائمة المهارات</h2>
                    <p>تعرف على قائمة المهارات الأكثر استخداماً</p>
                </div>
            </div>
        </div>

        <div class="row justify-content-lg-center">
           @foreach($skills as $con)
                @include('website.includes.skill')
            @endforeach
        </div>
        <div class="text-center mt-5">
            <a href="{{route('website.skills.index')}}" class="btn btn-main-outline rounded"
            >عرض جميع المهارات
                <i class="fa fa-angle-left prelative-3px ms-2"></i
                ></a>
        </div>
    </div>
</section>
@endif
@if(count($teachers) > 0)
<section class="team section-padding">
    <div class="container">
        <div class="row mb-100">
            <div class="col-lg-8 col-xl-8">
                <div class="section-heading text-center text-lg-start">
                    <h2 class="fs-20">أفضل المحاضرين</h2>
                    <p>تعرف على فريق عمل منصة فن للتعليم الفنى</p>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4">
                <div class="text-center text-lg-end">
                    <a href="{{route('website.teachers.index')}}" class="btn btn-main-outline rounded"
                    >عرض جميع المحاضرين
                        <i class="fa fa-angle-left prelative-3px ms-2"></i
                        ></a>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($teachers as $con)
                @include('website.includes.teacher')
            @endforeach
        </div>
    </div>
</section>
@endif

@endsection
