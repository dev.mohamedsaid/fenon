<!--start sidebar -->
<aside class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">

        <div>
            <h4 class="logo-text">منصة فن</h4>
        </div>
        <div class="toggle-icon ms-auto"> <i class="bi bi-list"></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        <li>
            <a href="{{ route('website.teacher.dashboard')  }}">
                <div class="parent-icon"><i class="bi bi-house-fill"></i>
                </div>
                <div class="menu-title">لوحة التحكم</div>
            </a>
        </li>


        <li>
            <a href="{{route('students_files.index')}}">
                <div class="parent-icon"><i class="bi bi-file-code-fill"></i>
                </div>
                <div class="menu-title"> ملفات الإجابات للطلبة</div>
                @if(getStudentFilesAnswers() > 0)
                <span class="countNumber">{{getStudentFilesAnswers()}}</span>
                @endif
            </a>
        </li>

        <li>
            <a href="{{route('courses_questions.index')}}">
                <div class="parent-icon"><i class="lni lni-library"></i>
                </div>
                <div class="menu-title"> أسألة الطلبة </div>
                @if(getQuestionsNotAnswered() > 0)
                <span class="countNumber">{{getQuestionsNotAnswered()}}</span>
                @endif
            </a>
        </li>




    </ul>
    <!--end navigation-->
</aside>
<!--end sidebar -->
