@extends('admin_dashboard.layout.master')
@section('Page_Title')   إجابات الطلاب للتقويم | {{$content->calendar?->title}}   @endsection
<style>
    .question_answer
    {
        background: #e9e9e9;
        padding: 25px;
        border-right: 2px solid var(--bs-blue);
    }
    .question_answer .label
    {
        background: white;
        padding: 2px 10px;
        /* color: #fff; */
        font-size: 9px;
        font-weight: bold;
        border-radius: 50px;
    }
    .text-successs i
    {
        top: 3px;
        font-size: 17px;
    }
    strong.text-danger
    {
        font-size: 11px;
    }
    .myInput
    {
        border: 2px solid var(--bs-teal);
        padding: 12px;
        outline: none;
    }
    .myDegreeLabel
    {
        border-radius: 7px;
        font-weight: bold;
    }
    .final_degree_circle
    {
        width: 150px;
        height: 150px;
        margin: 0 auto;
        border-radius: 50%;
        position: relative;
        overflow: hidden;
        font-size: 47px;
        border: 2px solid #7f7f7f;
        font-family: cursive;
    }
    .final_degree_circle .strong
    {
        display: block;
        border-bottom: 2px solid #7f7f7f;
    }
    label.error
    {
        font-weight: bold;
        position: absolute;
        bottom: -15px;
        left: 0;
    }
</style>
@section('content')

    <div class="row">
        <div class="col-lg-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <h5 class="mb-0"> <i class="lni lni-book"></i>     إجابات الطالب <small class="text-primary">({{$content->student?->name}})</small> للتقويم |   <small class="text-primary">({{$content->calendar?->title}})</small> </h5>
                    </div>
                    <div class="row g-3 mt-4">
                        <div class="col-12">
                            <div class="card shadow-none bg-light border">
                                <div class="card-body">
                                    <form class="row g-3" id="validateForm" method="post" enctype="multipart/form-data"
                                          action="{{route('calendar_answers.update', $content->id)}}">
                                        @method('put')
                                        @csrf


                                        <div class="col-md-6">
                                            <div class="card bg-light">
                                                <h4>بيانات الطالب</h4>
                                                <ul>
                                                    <li>
                                                       اسم الطالب : <strong>{{$content->student?->name}}</strong>
                                                    </li>
                                                    <li>
                                                         البريد الإلكتروني : <strong>{{$content->student?->email}}</strong>
                                                    </li>
                                                    <li>
                                                         الصف الدراسي : <strong>{{$content->student?->userInfo?->level?->title}} </strong>
                                                    </li>
                                                    <li>
                                                         المجموعة : <strong>{{$content->student?->userInfo?->group_type == 't' ? 'تجريبية':'ضابطة'}} </strong>
                                                    </li>
                                                    <li>
                                                         عدد مرات تسجيل الدخول : <strong class="text-primary">{{$content->student?->userInfo?->login_count}} </strong>
                                                    </li>
                                                    <li>
                                                        الوقت المستغرق للأمتحان : <strong class="text-primary">{{$content->duration}}  دقيقية</strong>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card bg-light">
                                                <h4>بيانات التقويم</h4>
                                                <ul>
                                                    <li>
                                                        اسم التقويم : <strong>{{$content->calendar?->title}}</strong>
                                                    </li>
                                                    <li>
                                                        نوع التقويم : <strong>{{$content->calendar?->type == 'final' ? 'نهائي': 'مرحلي'}}</strong>
                                                    </li>
                                                    @if($content->calendar?->type == 'final')
                                                        <li>
                                                            المنهج : <strong>{{$content->calendar?->curriculum?->title}}</strong>
                                                        </li>
                                                    @elseif($content->calendar?->type == 'staging' && $content->calendar?->staging_type == 'lesson')
                                                        <li>
                                                            تابع للدرس : <strong>{{$content->calendar?->lesson?->title}}</strong>
                                                        </li>
                                                    @elseif($content->calendar?->type == 'staging' && $content->calendar?->staging_type == 'course')
                                                        <li>
                                                            تابع للنشاط : <strong>{{$content->calendar?->course?->title}}</strong>
                                                        </li>
                                                    @endif
                                                    <li>
                                                        درجة التقويم : <strong>{{$content->calendar?->degree}} درجة</strong>
                                                    </li>
                                                    <li>
                                                        مدة التقويم : <strong>{{$content->calendar?->duration}} دقيقة</strong>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>


                                        @if(!is_null($content->student_final_degree))
                                        <div class="text-center">
                                            <h5 class="mb-3"> الدرجة النهائية</h5>
                                            @if($content->calendar_type == 'final')
                                                <h6>
                                                    @if($content->student_final_degree < ( ($content->calendar?->degree+50) /2))
                                                        <p class="fw-bold text-danger">راسب</p>
                                                    @else
                                                        <p class="fw-bold text-success">ناجح</p>
                                                    @endif
                                                </h6>
                                                <div class="final_degree_circle @if($content->student_final_degree < (($content->calendar?->degree+50) /2)) text-danger @else text-success @endif">
                                                    <strong class="strong" id="student_degree">
                                                        {{$content->student_final_degree}}
                                                    </strong>
                                                    <strong id="final_degree">{{$content->calendar?->degree+50}}</strong>
                                                </div>
                                            @else
                                                <h6>
                                                    @if($content->student_final_degree < ($content->calendar?->degree /2))
                                                        <p class="fw-bold text-danger">راسب</p>
                                                    @else
                                                        <p class="fw-bold text-success">ناجح</p>
                                                    @endif
                                                </h6>
                                                <div class="final_degree_circle @if($content->student_final_degree < ($content->calendar?->degree /2)) text-danger @else text-success @endif">
                                                    <strong class="strong" id="student_degree">
                                                        {{$content->student_final_degree}}
                                                    </strong>
                                                    <strong id="final_degree">{{$content->calendar?->degree}}</strong>
                                                </div>
                                            @endif


                                        </div>
                                        @else

                                        <div class="col-12 mt-4">
                                            <div class="row px-3">
                                                <div class="col-12 ">
                                                    <h4 class="mb-5 fw-bold">تصحيح الإجابات :</h4>
                                                </div>
                                                @if($content->calendar?->type == 'final')
                                                <div class="col-md-4">
                                                    <label class="form-label">  درجة عدد مرات تسجيل الدخول للطالب <span class="text-danger">*</span> </label>
                                                    <div class="position-relative d-flex justify-content-around align-items-center">
                                                        <input type="number" min="0" max="5" name="login_degree" class="form-control sumDegrees" required placeholder="ادخل الدرجة">
                                                        <label class="myDegreeLabel bg-black w-100 text-white p-2">\ 5</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-label">  درجة الحضور والتكليفات <span class="text-danger">*</span> </label>
                                                    <div class="position-relative d-flex justify-content-around align-items-center">
                                                        <input type="number"  min="0" max="20" name="attendance_and_mission_degree" class="form-control sumDegrees" required placeholder="ادخل الدرجة">
                                                        <label class="myDegreeLabel bg-black w-100 text-white p-2">\ 20</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-label">  درجة التقويمات المرحلية <span class="text-danger">*</span> </label>
                                                    <div class="position-relative  d-flex justify-content-around align-items-center">
                                                        <input type="number" readonly value="{{$stagingDegrees}}"  min="0" max="25" name="staging_calendars_degree" class="form-control sumDegrees" required placeholder="ادخل الدرجة">
                                                        <label class="myDegreeLabel bg-black w-100 text-white p-2">\ 25</label>
                                                    </div>
                                                </div>
                                                @endif
                                                <input type="hidden" value="{{$content->calendar?->degree}}" name="calendar_final_calendar_degree">
                                            </div>
                                        </div>

                                        <div class="col-12 mt-4">

                                            <h5 class="text-primary"><i class="bi bi-question-lg mx-2"></i>
                                                الأسئلة والإجابات:</h5>


                                            @foreach($answers as $key=>$answer)
                                            <div class="question_answer m-4">
                                                <div class="position-relative  d-lg-flex justify-content-between align-items-center">
                                                    <h6>{{$key+1}} - {{$answer->question_title}}</h6>
                                                    <input type="number" min="0"
                                                           name="question_degree[{{$answer->id}}]" placeholder="ادخل درجة السؤال بالأرقام" class="mx-2 myInput sumDegrees" required />
                                                </div>

                                                @if($answer->question_kind == 'theoretical')
                                                <span class="label">
                                                    @if($answer->question_type == 'single_choice')
                                                        اختيار فردي
                                                    @elseif($answer->question_type == 'multiple_choice')
                                                        اختيار متعدد
                                                    @else
                                                        صح أو خطأ
                                                    @endif
                                                </span>
                                                @endif
                                                <span class="mx-1"></span>
                                                <span class="label">{{$answer->question_kind == 'practical' ? 'عملي' : 'نظري'}}</span>
                                                <h6 class="mt-3 text-danger">إجابة الطالب :</h6>
                                                <h6 class="">
                                                    @if($answer->question_kind == 'practical')
                                                        @if($answer->video_links)
                                                            <h6 class="my-3">الروابط التي تم وضعها من الطالب :</h6>
                                                            <ul>
                                                               @foreach(json_decode($answer->video_links) as $key=>$value)
                                                                   @foreach($value as $val)
                                                                    <li>
                                                                        <a href="{{$val}}" target="_blank">{{$val}}</a>
                                                                    </li>
                                                                    @endforeach
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                        @if($answer->answer)
                                                        <img src="{{$answer->answer}}" width="100%" />
                                                        @endif
                                                        @if(!$answer->video_links && !$answer->answer)
                                                            <strong class="text-danger"> السؤال غير مجاوب عليه</strong>
                                                        @endif
                                                    @else
                                                        @if($answer->question_type == 'single_choice')

                                                            @if($answer->answer != '' || !is_null($answer->answer))

                                                                @foreach($answer->question?->choices as $choice)
                                                                    @if($choice->id == $answer->answer)
                                                                        <strong class="text-successs"> * {{$choice->choice_text}}</strong>
                                                                        @if($choice->choice_file)
                                                                            - <a href="{{assetURLFile($choice->choice_file)}}" download>الملف </a>
                                                                        @endif
                                                                        @if($choice->choice_video_url)
                                                                            - <a href="{{$choice->choice_video_url}}" target="_blank">الفيديو </a>
                                                                        @endif
                                                                    @endif
                                                                @endforeach

                                                            @else
                                                                <strong class="text-danger"> السؤال غير مجاوب عليه</strong>
                                                            @endif

                                                        @elseif($answer->question_type == 'multiple_choice')
                                                            @if($answer['answer'] != ''  || !is_null($answer['answer']))
                                                            <strong class="text-successs">
                                                                @foreach(json_decode($answer['answer']) as $val)
                                                                    <ul>
                                                                        <li>
                                                                            @foreach($answer->question?->choices as $choice)
                                                                                @if($choice->id == $val)
                                                                                    <strong class="text-successs">  {{$choice->choice_text}}</strong>
                                                                                    @if($choice->choice_file)
                                                                                        - <a href="{{assetURLFile($choice->choice_file)}}" download>الملف </a>
                                                                                    @endif
                                                                                    @if($choice->choice_video_url)
                                                                                        - <a href="{{$choice->choice_video_url}}" target="_blank">الفيديو </a>
                                                                                    @endif
                                                                                @endif
                                                                            @endforeach
                                                                        </li>

                                                                    </ul>
                                                                @endforeach
                                                            </strong>
                                                            @else
                                                                <strong class="text-danger"> السؤال غير مجاوب عليه</strong>
                                                            @endif
                                                        @else
                                                            @if($answer->answer != '' || !is_null($answer->answer))
                                                            <strong class="text-successs">
                                                                @if($answer->answer == '1')
                                                                    <i class="bx bxs-check-circle position-relative  mx-2"></i> صح
                                                                @else
                                                                    <i class="bx bx-window-close position-relative  mx-2"></i> خطأ
                                                                @endif
                                                            </strong>
                                                            @else
                                                                <strong class="text-danger"> السؤال غير مجاوب عليه</strong>
                                                            @endif
                                                        @endif
                                                    @endif

                                                </h6>
                                            </div>
                                            @endforeach




                                            <div class="col-md-4  mx-auto mt-4">
                                                <button type="submit" class="btnIcon btn btn-success px-5 w-100">
                                                    <i class="bx bx-edit-alt"></i>
                                                    تأكيد التصحيح
                                                </button>
                                            </div>


                                        </div>

                                        @endif




                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><!--end row-->
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $(document).ready(function () {
            $("#validateForm").validate({
                rules: {
                    login_degree: {
                        required: true,
                    },
                    attendance_and_mission_degree: {
                        required: true,
                    },
                    staging_calendars_degree: {
                        required: true,
                    },
                    'question_degree[]': {
                        required: true,
                    }

                },
                messages: {
                    login_degree: {
                        required: "الحقل مطلوب",
                    },
                    attendance_and_mission_degree: {
                        required: "الحقل مطلوب",
                    },
                    staging_calendars_degree: {
                        required: "الحقل مطلوب",
                    },
                    'question_degree[]': {
                        required: "الحقل مطلوب",
                    },

                }
            });
        });




    </script>
@endpush
