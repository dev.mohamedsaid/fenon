<?php

namespace App\Http\Controllers\StudentControllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\StudentRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Http\Traits\HelperTrait;
use App\Models\Level;
use App\Models\StudentAnswer;
use App\Models\StudentAnswerDegree;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    use HelperTrait;

    public function dashboard()
    {
        $page_title = 'تعديل البيانات الشخصية';
        $levels = Level::whereStatus('yes')->orderBy('sort', 'asc')->pluck('id', 'title');
        return view('website.students.dashboard.dashboard', compact('levels','page_title'));
    }

    public function update(StudentUpdateRequest $request)
    {
        $data = $request->validated();
        DB::beginTransaction();
        try {
            $user = User::find(auth()->user()->id);
            $user->update([
                'name' =>$data['name'],
            ]);
            $this->createUserInfo($data,$user->id,$request, $type='updated');
            DB::commit();
            toastr()->success('تم تعديل بياناتك بنجاح', 'نجح', ['timeOut' => 5000]);
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            toastr()->error($this->error, 'فشل', ['timeOut' => 5000]);
            return redirect()->back();
        }
    }




    public function myCalendars()
    {
        $page_title = 'تقويماتي';
        $myFinalCalendarAndHisStaging = StudentAnswerDegree::with(['calendar:id,title', 'curriculum:id,title'])
            ->where('student_id' , auth()->user()->id)
            ->where('calendar_type', 'final')->get();

        $myCalendars = $myFinalCalendarAndHisStaging->map(function($calendarDegree, $key) {
           $stagingCalendarsOfAuthStudent = StudentAnswerDegree::with(['calendar:id,title'])
               ->where('student_id' , auth()->user()->id)
               ->where('calendar_type', 'staging')->where('curriculum_id', $calendarDegree->curriculum?->id)->get();
            return [
                'id' => $calendarDegree->id,
                'curriculum_title' => $calendarDegree->curriculum?->title,
                'calendar_title' => $calendarDegree->calendar?->title,
                'student_final_degree' => $calendarDegree->student_final_degree,
                'duration' => $calendarDegree->duration,
                'staging' => $stagingCalendarsOfAuthStudent
            ];
        });

        return view('website.students.dashboard.calendars', compact('myCalendars','page_title'));
    }


}


