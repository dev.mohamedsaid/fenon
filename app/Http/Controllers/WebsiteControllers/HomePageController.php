<?php

namespace App\Http\Controllers\WebsiteControllers;

use App\Http\Controllers\Controller;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Http\Request;

class HomePageController extends Controller
{

    //Index
    public function index()
    {
        $skills = Skill::with('images')->whereStatus('yes')->orderBy('sort', 'asc')->limit(4)->get();
        $teachers = User::with('userInfo')->whereType(2)->whereNotNull('email_verified_at')->limit(4)->get();
        return view('website.index', compact('skills', 'teachers'));
    }


}
