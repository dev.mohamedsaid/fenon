<?php

namespace App\Http\Controllers\AdminControllers;
use App\Models\Category;
use App\Models\Course;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $students = User::whereType(3)->count();
        $teachers = User::whereType(2)->count();
        $courses = Course::whereStatus('yes')->count();
        $categories = Category::whereStatus('yes')->count();
        return view('admin_dashboard.dashboard', compact('students', 'teachers','courses','categories'));
    }

}
