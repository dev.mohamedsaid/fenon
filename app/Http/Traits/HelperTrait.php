<?php

namespace App\Http\Traits;

use App\Models\CalendarQuestionChoice;
use App\Models\ModuleFile;
use App\Models\UserInfo;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

Trait HelperTrait
{
    public $insertMsg = ' تم إنشاء العنصر بنجاح ';
    public $updateMsg = 'تم تحديث العنصر بنجاح';
    public $deleteMsg = 'تم حذف العنصر بنجاح';
    public $error = 'يوجد مشكلة ما';

    public $paginate = 50;

    //Main Upload File Method
    public function upload_file_helper_trait($request,$fileInputName, $moveTo)
    {
        $file = $request->file($fileInputName);
        $fileUploaded=rand(1,99999999999).'__'.$file->getClientOriginalName();
        $file->move($moveTo, $fileUploaded);
        return $fileUploaded;
    }

    public function createUserInfo($data,$userID, $request, $type)
    {

        $someData = [
            'user_id'=>$userID,
            'phone' =>$data['phone'],
            'group_type' =>isset($data['group_type']) ? $data['group_type'] : 't',
            'job_title' =>$data['job_title'],
            'gender' =>isset($data['gender']) ? $data['gender'] : 'male',
            'national_id'=>$data['national_id'],
            'city'=>$data['city'],
            'specialist' =>$data['specialist'],
            'qualification'=>$data['qualification'],
            'school_or_college'=>$data['school_or_college'],
            'department'=>$data['department'],
            'reason'=>isset($data['reason']) ? $data['reason']:'',
            'status'=>isset($data['status']) ? 'yes' : 'no',
            'level_id'=>isset($data['level_id']) ? $data['level_id'] : null,
        ];
        if($request->file('image'))
        {
            $image = $this->upload_file_helper_trait($request,'image', 'uploads/');
            $someData['image']=$image;
        }

        if($type == 'created')
        {
            UserInfo::create($someData);
        }
        elseif($type == 'updated')
        {
            UserInfo::where('user_id', $userID)->update($someData);
        }
    }



    public function saveMultipleFiles($module, $module_id , $data, $moveTo)
    {
        foreach ($data['name'] as $key => $value)
        {
            $fileUploaded=rand(1,99999999999).'__'.$data['file_uploaded'][$key]->getClientOriginalName();
            $data['file_uploaded'][$key]->move($moveTo, $fileUploaded);
            $moduleFiles =  [
                'module_name' =>$module,
                'module_id' =>$module_id,
                'name' =>$value,
                'file_type' =>$data['file_type'][$key],
                'file_uploaded'=>$fileUploaded
            ];
            ModuleFile::create($moduleFiles);
        }
    }


    //SaveQuestionChoices
    public function SaveQuestionChoices($question_id , $data, $moveTo)
    {
        if($data['question_type'] != 'true_false')
        {
            foreach ($data['choice_text'] as $key => $value)
            {
                $insertChoices =  [
                    'question_id' =>$question_id,
                    'choice_text' =>$value,
                    'choice_video_url' =>$data['choice_video'][$key],
                ];

                if(isset($data['choice_file'][$key]))
                {
                    $fileUploaded=rand(1,99999999999).'__'.$data['choice_file'][$key]->getClientOriginalName();
                    $data['choice_file'][$key]->move($moveTo, $fileUploaded);
                    $insertChoices['choice_file'] = $fileUploaded;
                    $insertChoices['choice_file_ext'] = pathinfo($fileUploaded, PATHINFO_EXTENSION);
                }
                CalendarQuestionChoice::create($insertChoices);
            }
        }

    }



}

